# A  keymap for xwhatsit/brand_new_model_f/f77/$(CONTROLLER)

This is the default keymap for the HHKB Split Shift and Split Backspace, and it has 0-9 and cursor keys on the right side block

make command for this layout is:

```
make xwhatsit/brand_new_model_f/f77:jackhill
```

