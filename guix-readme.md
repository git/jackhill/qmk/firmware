The following command produces an environment suitable for building
firmware for AVR keyboards.

TODO: make this an actual manifest, or better yet put the whole build
process into Guix 😀

```
guix environment -C --ad-hoc coreutils make avr-toolchain \
python python-appdirs python-argcomplete python-colorama \
python-nose2 python-flake8 python-pep8 findutils sed grep \
diffutils bash which gawk
```

To install the fimrware:
* `guix environment --ad-hoc dfu-programmer`
* both shifts and B to enter bootloader
* `sudo dfu-programmer atmega32u2 erase`
* `sudo dfu-programmer atmega32u2 flash FILENAME.hex`

